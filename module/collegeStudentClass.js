import { Student } from "./studentClass.js";

export class CollegeStudent extends Student{
    constructor (hoTen, ngaySinh, queQuan, tenTruong, lop, sdt, chuyenNganh, mssv) {
        super(hoTen, ngaySinh, queQuan, tenTruong, lop, sdt);
        this.chuyenNganh = chuyenNganh ;        
        this.mssv = mssv ;
    }
}