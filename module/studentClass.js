import { Person } from "./personClass.js";

export class Student extends Person{
    constructor (hoTen, ngaySinh, queQuan, tenTruong, lop, sdt) {
        super(hoTen, ngaySinh, queQuan);
        this.tenTruong = tenTruong ;        
        this.lop = lop ;        
        this.sdt = sdt ;        
    }
}