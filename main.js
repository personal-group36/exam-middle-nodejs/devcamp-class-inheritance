import { Person } from "./module/personClass.js";
var person = new Person("ha","12-17-12", "hanoi");
console.log (person);

import { Student } from "./module/studentClass.js";
var student = new Student("ho", "12-17-05", "hochiminh", "tqt", 12, 131313131);
console.log (student);

import { CollegeStudent } from "./module/collegeStudentClass.js";
var collegeStudent = new CollegeStudent("ho", "12-17-05", "hochiminh", "tqt", 12, 131313131, "it", 1234);
console.log (collegeStudent);

import { Worker } from "./module/workerClass.js";
var worker = new Worker("ha","12-17-12", "hanoi", "xay dung", "ctxd1", 10000000);
console.log (worker);